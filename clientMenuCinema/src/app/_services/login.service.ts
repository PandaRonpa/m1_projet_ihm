import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';
import * as firebase from 'firebase/app';
import { BehaviorSubject, Subscription } from 'rxjs';
import { CommandService } from './command.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isConnected: BehaviorSubject<boolean> = new BehaviorSubject(false);
  profilePicture: string;
  userSubscription: Subscription;

  constructor(private auth: AngularFireAuth, private cmds: CommandService) {}

  authentificate() {
    this.userSubscription = this.auth.user.subscribe( async (u: User) => {
      // On contacte le serveur métier pour l'informer si un nouvel utilisateur existe :
      if (u !== null) {
        this.profilePicture = u.photoURL;
        console.log('L’utilisateur Firebase est ', u.displayName);
        this.cmds.dbContainsClient(u).subscribe(result =>{
          var tmp = u.displayName.split(" ");
          if(!result){this.cmds.saveClient(u.email, tmp[0], tmp[1])}
          this.cmds.getdbCommandList(null, u.email, 2);
        });
        // On broadcast l'evenement quand l'utilisateur est connecté
        this.isConnected.next(true);
      }
    });
  }

  async logOut() {
    this.auth.signOut().then( () => {
      console.log('Sign-out successful');
      this.userSubscription.unsubscribe();
      this.isConnected.next(false);
    });
  }

  loginInGoogle() {
    const provider = new firebase.auth.GoogleAuthProvider();
    this.auth.signInWithPopup(provider);
    this.isConnected.next(true);
  }
}
