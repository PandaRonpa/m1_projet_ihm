import { AfterContentChecked, Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { Client, Command, ListClient, ListCommand } from '../_interfaces/command'
import { AngularFireDatabase } from '@angular/fire/database';
import { Film } from './basket.service';
import { DataSnapshot } from '@angular/fire/database/interfaces';
import { User } from 'firebase';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { first, map } from 'rxjs/operators';
import { LoginService } from './login.service';

/**
 * @public @class
 * @namespace _services
 * @description Special service in order to save the command
 * of the users into the database of firebase and get them
 * @author Bastien andre - Avanzino Aurélien
 * @since 06/01/2021
 * @version 1.0
 */
@Injectable({
  providedIn: 'root'
})
export class CommandService {

  /*********Variables*********/
  private date: Date = new Date();
  private client: Client;
  private listCommand: ListCommand;
  private nbTodayCommand:number;
  private idClient: string;
  private firestoreClients: AngularFirestoreCollection<Client>;
  private firestoreCommands: AngularFirestoreCollection<Command>;

  /**
   * @public @constructor
   * @description 
   * @param auth - AngularFireAuth
   * @param db - AngularFireDatabase
   * @since 06/01/2021
   */
  constructor(private firestore: AngularFirestore){
    this.initObject();
    this.initParameters();
    /*this.auth.user.subscribe( u => {
      if(u !== null){
        var tmp = u.displayName.split(" ");
        this.dbContainsClient(u).subscribe(async result =>{
          if(!result){this.saveClient(u.email, tmp[0], tmp[1])}
          await this.getdbCommandList(null, u.email, 2);
          console.log(this.client);
          console.log(this.listCommand);
        });
      }
    });*/
  }

  /**
   * @public @method
   * @description
   * @since 20/01/2021
   */
  public reset(): void{
    this.initParameters();
    this.initObject();
  }

  /**
   * @private @method
   * @description Initialize the parameters of the service
   * @since 20/01/2021
   */
  private initParameters(): void{
    this.nbTodayCommand = 0;
    this.idClient = null;
    this.firestoreClients = this.firestore.collection('/Clients');
    this.firestoreCommands = this.firestore.collection('/Commands');
  }

  /**
   * @private @method
   * @description Initialize the JSON Object of the service
   * @since 20/01/2021
   */
  private initObject(): void {
    this.client = {id: null,name: null,famName: null,email: null,emailVerified: false,dateCreation: null};  
    this.listCommand = {clientId: "",commands: [],totalCommands: 0,totalPages: 0};
  }

  /*******************FONCTION D'ENREGISTREMENT EN BDD*************************/

  /**
   * @public @async @method
   * @description Save the command of the client in the database
   * @param command - The list of films command by the client
   * @param price - Total price of the command
   * @returns True if the command is saved successfully in the database
   * @since 06/01/2021
   */
  public async saveCommand(command: Film[], price: number): Promise<boolean>{
    var newCommand: Command = { clientId: this.idClient, date: this.dateNowToString(),
      films: command,prixTotal: price}
    var newDoc: DocumentReference = await this.firestoreCommands.add(newCommand);
    /*On met à jour la liste locale pour faire le moins d'appel à la BDD possible*/
    this.listCommand.commands.push(newCommand);
    this.listCommand.totalCommands += 1;
    this.listCommand.totalPages += this.listCommand.totalCommands % 20 === 0 ? 1 : 0;
    this.nbTodayCommand += 1;
    return newDoc !== null ? true : false;
  }

  /**
   * @public @method
   * @description Save the client account in the database
   * @summary Use when the client connects into the website for the first
   * time. We save him in the database
   * @returns True if the command is saved successfully in the database
   * @since 09/01/2021
   */
  public saveClient(email: string, name?: string, famName?: string): boolean{
    console.log("Le client n'existe pas. Enregistrement dans la BDD !");
    var result = false;
    var idclient = this.firestore.createId();
    this.idClient = idclient;
    this.client = {
      id: idclient,
      name: name,
      famName: famName,
      email: email,
      emailVerified: false,
      dateCreation: this.dateNowToString()
    }
    var client = this.firestoreClients.doc<Client>(this.idClient);
    client.set(this.client).then((sucess)=>{result = true;}, (rejected)=>{result = false});
    return result;
  }

  /*******************FONCTION DE RECUPERATION DES DONNEES*************************/

  public getIdClient(): string{
    return this.idClient;
  }



  /**
   * @public @methode
   * @description Returns all the command in fonction of the ID Client
   * @param clientId - Id of the user
   * @returns ListCommand - La liste des commandes du client actuel connecté
   * @since 06/01/2021
   */
  public getcommandsList() : ListCommand{
    return this.listCommand; 
  }

  /**
   * @private @method
   * @description Returns all the command in fonction of the ID Client
   * @param clientId - Id of the user
   * @returns ListCommand - All the command made by the user given in parameter
   * @since 06/01/2021
   */
  //TODO rajouter les conditions pour prendre en compte nbCommandParPage
  public async getdbCommandList(id?: string, email?:string, nbCommandParPage?: number) : Promise<ListCommand>{
    var result: ListCommand = {
      clientId: this.idClient,
      commands: [],
      totalCommands: 0,
      totalPages: 0
    };
    var collection = (await this.firestoreCommands.ref.get()).docs;
    collection.forEach(command => {
      if(command.get('clientId') === this.idClient){
        result.commands.push({id: command.get('id'),clientId: command.get('clientId'),
          prixTotal: command.get('prixTotal'),films: command.get('films'),date: command.get('date')});
        result.totalCommands += 1;
        result.totalPages += result.totalCommands % nbCommandParPage === 0 ? 1 : 0;
      }
    });
    this.listCommand = result; //Pour éviter de refaire à chaque fois appel à la BDD
    return result;
  }

  public dbContainsClient(client: User): Observable<boolean>{
    var result = false;
    var collection: Observable<boolean> = this.firestoreClients.get({source: 'server'}).pipe(first(), map(listClient => {
      listClient.forEach(bdClient => {
        if(bdClient.get('email') === client.email){
          this.idClient = bdClient.id;
          this.client = {id: bdClient.get('id'),name: bdClient.get('name'),famName: bdClient.get('famName'),
            email: bdClient.get('email'),dateCreation: bdClient.get('dateCreation')};
          result = true;
        }
      })
      return result;
    }));
    return collection;
  }

  /**
   * @public @method
   * @description Return the client in function of the email
   * We use only the email because it's a unique identifier in database
   * @param email - Email du compte client
   * @returns The Promise of Client if it's present in the database 
   * @since 10/01/2021
   */
  public async getdbClient(id?: string, email?: string): Promise<Client>{
    var result: Client;
    var collection = (await this.firestoreClients.ref.get()).docs;
    collection.forEach(client => {
      if(client.get('id') === id || client.get('email') === email){
        console.log("Le client existe dans la BDD : ", client);
        this.client = {id: client.get('id'),name: client.get('name'),famName: client.get('famName'),
          email: client.get('email'),emailVerified: client.get('emailVerified')};
      }
    });
    return this.client;
  }

  /**
   * 
   * @param name 
   * @param famName 
   * @param emailVerified 
   */
  public async getdbListClient(name?: string, famName?: string, emailVerified?: boolean): Promise<ListClient>{
    var result:ListClient;
    var collection = (await this.firestoreClients.ref.get()).docs;
    collection.forEach(client => {
      //TODO
    });
    return result;
  }

  /**************FUNCTION OUTILS POUR BDD******************/

  private createNumCommand(): string{
    var result = "";

    return null;
  }

  /**
   * @private @method
   * @description Give the date at the instant t in the format
   * YYYY-MM-DD HH:mm:ss
   * @returns The Date at the moment where the method is called
   * @see CommandService.saveCommand();
   * @since 09/01/2021
   */
  private dateNowToString(): string{
    var day = this.date.getDate();
    var month = this.date.getMonth()+1; var hour = this.date.getHours();
    var minute = this.date.getMinutes(); var second = this.date.getSeconds();
    var result = ""+this.date.getFullYear()+"-";
    result += (day < 10) ? "0"+day : day;
    result += (month < 10) ? "-0"+month : "-"+month;
    result += (hour < 10) ? " 0"+hour : " "+hour;
    result += (minute < 10) ? ":0"+minute : ":"+minute;
    result += (second < 10) ? ":0"+second : ":"+second;
    return result;
  }

  /**
   * @private @method
   * @description Convert a string in ASCII format
   * @param text The text to convert in ASCII
   * @returns The text converted in ASCII
   * @since 09/01/2021
   * @deprecated
   */
  private stringToASCII(text: string): string{
    var result = text.toUpperCase();
    var accent = ["À", "Á", "Â", "Ä", "Ç", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ñ", "Ó", "Ò", "Ô", "Ö", "Ú", "Ù", "Û", "Ü"];
    var saccent= ["A", "A", "A", "A", "C", "E", "E", "E", "E", "I", "I", "I", "I", "N", "O", "O", "O", "O", "U", "U", "U", "U"];
    var int = 0;
    accent.forEach(char =>{text.replace(char, saccent[int]);int++;});
    return result.trim();
  }
}
