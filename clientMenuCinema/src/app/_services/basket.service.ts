import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

export interface Film {
  id: number;
  title: string;
  posterPath: string;
  price: number;
  quantity: number;
}

@Injectable({
  providedIn: 'root'
})
export class BasketService {
  panierObservable: Observable<Film[]>;

  // Voir https://www.npmjs.com/package/@angular/fire
  /* Votre base Firestore
   *   Collections :
   *     - Panier
   *       - Document {id: string}
   *     - Lists
   *       - Document {label: string, films: string[]} avec films un tableau d'ID de films
   */
  constructor(private afs: AngularFirestore) {
    this.panierObservable = afs.collection<Film>('Panier').valueChanges();
  }

  appendFilmToBasket(idFilm: number, titleFilm: string,
                     posterPathFilm: string, priceFilm: number, quantityFilm: number) {
    this.afs.collection<Film>('Panier').doc(idFilm.toString()).set(
      {id: idFilm, title: titleFilm, posterPath: posterPathFilm, price: priceFilm, quantity: quantityFilm}
    );
  }

  removeFilmFromBasket(idFilm: number) {
    this.afs.collection('Panier').doc(idFilm.toString()).delete().then( () => console.log('Document successfully deleted!') ).catch(
      (error) => console.error('Error removing document: ', error));
  }
}
