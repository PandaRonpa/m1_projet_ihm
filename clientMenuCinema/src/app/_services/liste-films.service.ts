import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Film } from './basket.service';

@Injectable({
  providedIn: 'root'
})

export class ListeFilmsService {

  listsObservable: Observable<ListFilms[]>;

  constructor(private afs: AngularFirestore) { 
    this.listsObservable = afs.collection<ListFilms>('ListeFilms').valueChanges();
  }

  /*
    let filmsTest: FILM[] = new Array();
    filmsTest.push({id: 1, title: "titletest", posterPath: 'testpath', price: 5, quantity: 1});
    filmsTest.push({id: 2, title: "titletest2", posterPath: 'testpath2', price: 2, quantity: 1});
    liste.appendListeFilms( {label: "ListeTestststs", films: filmsTest } );
  */
  appendListeFilms(ListFilms: ListFilms) {
      this.afs.collection<ListFilms>('ListeFilms').doc(ListFilms.Label).set(
        {Label: ListFilms.Label, films: ListFilms.films}
      );
  }

  removeListeFilms(ListFilms: ListFilms) {
    this.afs.collection('ListeFilms').doc(ListFilms.Label).delete().then( () => console.log('Document successfully deleted!') ).catch(
      (error) => console.error('Error removing document: ', error));
  }

  appendFilmToList(film: Film, ListFilms: ListFilms) {
    ListFilms.films.push(film);
    this.afs.collection<ListFilms>('ListeFilms').doc(ListFilms.Label).set(
      {Label: ListFilms.Label, films: ListFilms.films}
    );
  }

  removeFilmFromList(film: Film, ListFilms: ListFilms) {
    const index = ListFilms.films.indexOf(ListFilms.films.find(element => element.id == film.id));
    if (index > -1) {
      ListFilms.films.splice(index, 1);
    }
    this.afs.collection<ListFilms>('ListeFilms').doc(ListFilms.Label).update({
      films: ListFilms.films
    })
  }
}

export interface ListFilms {
  Label: string;
  films: Film[];
}
