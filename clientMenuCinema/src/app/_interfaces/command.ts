import { Film } from "../_services/basket.service";

export interface Command {
    id?: string;
    numero?: string;
    clientId: string;
    clientName?: string;
    date: string;
    films: Film[];
    prixTotal: number;
}

export interface ListCommand{
    clientId: string;
    commands: Command[];
    totalCommands?: number;
    totalPages?: number;
}

export interface Client{
    id: string;
    name?: string;
    famName?: string;
    nameACSII?: string;
    famNameASCII?: string;
    email: string;
    emailVerified?: boolean;
    dateCreation?: string;
}

export interface ListClient{
    clients: Client[];
    totalClients?: number;
    totalPages?: number;
}
