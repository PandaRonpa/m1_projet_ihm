import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { registerLocaleData, DatePipe, DecimalPipe } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeFrExtra from '@angular/common/locales/extra/fr';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

// Material
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { MatDividerModule } from '@angular/material/divider';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatRippleModule } from '@angular/material/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatStepperModule } from '@angular/material/stepper';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatExpansionModule } from '@angular/material/expansion';
import {MatCheckboxModule} from '@angular/material/checkbox';

// Services
import { TmdbService } from './tmdb.service';
import { BasketService } from './_services/basket.service';

// Components
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { NavigationComponent } from './navigation/navigation.component';
import { HomeComponent } from './home/home.component';
import { BasketComponent } from './basket/basket.component';
import { BasketLineComponent } from './basket-line/basket-line.component';
import { FilmsComponent } from './films/films.component';
import { LogoutComponent } from './logout/logout.component';
import { CommandComponent } from './command/command.component';
import { DialogListComponent } from './dialog-list/dialog-list.component';
import { ListFilmsComponent } from './list-films/list-films.component';
import { DialogComponent } from './dialog/dialog.component';

registerLocaleData(localeFr, 'fr-FR', localeFrExtra);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProfileComponent,
    NavigationComponent,
    HomeComponent,
    BasketComponent,
    BasketLineComponent,
    FilmsComponent,
    CommandComponent,
    LogoutComponent,
    DialogListComponent,
    ListFilmsComponent,
    DialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MatFormFieldModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatToolbarModule,
    MatSnackBarModule,
    MatMenuModule,
    MatCardModule,
    ReactiveFormsModule,
    MatInputModule,
    InfiniteScrollModule,
    MatIconModule,
    Ng2SearchPipeModule,
    MatTabsModule,
    MatBadgeModule,
    MatDialogModule,
    MatDividerModule,
    DragDropModule,
    MatRippleModule,
    MatTooltipModule,
    MatStepperModule,
    MatListModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatExpansionModule
  ],
  providers: [TmdbService, HttpClient, BasketService,
    { provide: LOCALE_ID, useValue: 'fr-FR' },
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 2000 } 
  },
    DatePipe, DecimalPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
