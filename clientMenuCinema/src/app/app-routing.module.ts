import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ProfileComponent } from './profile/profile.component';
import { HomeComponent } from './home/home.component';
import { BasketComponent } from './basket/basket.component';
import { FilmsComponent } from './films/films.component';
import { CommandComponent } from './command/command.component';
import { ListFilmsComponent } from './list-films/list-films.component';


const routes: Routes = [
  { path: '', component: AppComponent, data: {title: 'Home - MenuCinema'}},
  { path: 'home', component: HomeComponent, data: {title: 'Home - MenuCinema'}},
  { path: 'profile', component: ProfileComponent, data: {title: 'Profil - MenuCinema'}},
  { path: 'basket', component: BasketComponent, data: {title: 'Panier - MenuCinema'}},
  { path: 'films', component: FilmsComponent, data: {title: 'Films - MenuCinema'}},
  { path: 'command', component: CommandComponent, data: {title: 'Commande - MenuCinema'}},
  { path: 'listFilms', component: ListFilmsComponent, data: {title: 'Liste Films - MenuCinema'}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
