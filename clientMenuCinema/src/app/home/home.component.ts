import { Component } from '@angular/core';
import { MovieResponse } from '../tmdb-data/Movie';
import { TmdbService } from '../tmdb.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  popularMovies: MovieResponse[] = [];

  constructor(private tmdb: TmdbService) {
    this.tmdb.init(environment.tmdbKey);
    this.initMovies();
  }

  async initMovies() {
    const popMovies = (await this.tmdb.getPopularMovies()).results;
    const L: (void | MovieResponse)[] = (await Promise.all(popMovies.map( film =>
      this.tmdb.getMovie(+film.id).catch(error => console.error(film) ) ) ) ).filter(v => v !== undefined);
    this.popularMovies = L as MovieResponse[];
  }
}
