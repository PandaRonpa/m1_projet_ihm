import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {
  diplayName: string;
  email: string;
  phoneNumber: string;
  photoURL: string;
  uid: string;

  constructor(private auth: AngularFireAuth) {
    auth.user.subscribe( async u => {
      if (u !== null) {
        console.log('Récupération du profil de :', u);
        this.diplayName = u.displayName;
        this.email = u.email;
        this.phoneNumber = u.phoneNumber;
        this.photoURL = u.photoURL;
        this.uid = u.uid;
      }
    });
  }
}
