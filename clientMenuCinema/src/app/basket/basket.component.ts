import { Component } from '@angular/core';
import { CommandService } from '../_services/command.service';
import { BasketService, Film } from '../_services/basket.service';
import { LoginService } from '../_services/login.service';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.scss']
})

export class BasketComponent {
  panier: Film[] = [];
  isConnected: boolean;

  constructor(private backetService: BasketService, private loginService: LoginService, private commandService: CommandService) {
    backetService.panierObservable.subscribe(panier => this.panier = panier);
    loginService.isConnected.subscribe(value => {this.isConnected = value; });
  }

  supprimerArticle(code: number) {
    this.backetService.removeFilmFromBasket(code);
  }

  getPrixPanier(): number {
    let prixPanier = 0;
    for (const film of this.panier) {
      prixPanier += film.price;
    }
    return prixPanier;
  }

  deleteBasket() {
    for(const movie of this.panier) {
      this.supprimerArticle(movie.id);
    }
  }

  /**
   * @public @method
   * @description Send to the command service the basket
   * in order to save it in the database
   * @since 06/01/2020
   * @see {@link commandService}
   */
  async passerCommande() {
    var result = await this.commandService.saveCommand(this.panier, this.getPrixPanier());
    if(result){this.deleteBasket();}
  }
}
