import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Film } from '../_services/basket.service';
import { ListeFilmsService, ListFilms } from '../_services/liste-films.service';

@Component({
  selector: 'app-dialog-list',
  templateUrl: './dialog-list.component.html',
  styleUrls: ['./dialog-list.component.scss']
})
export class DialogListComponent {

  listesFilms: ListFilms[] = []
  checked: boolean[] = []
  film: Film

  constructor(public lfs: ListeFilmsService, @Inject(MAT_DIALOG_DATA) public data, private dialogRef: MatDialogRef<DialogListComponent>) {
    this.film = data.dataKey
    lfs.listsObservable.subscribe(value => {
      this.listesFilms = value

      for (let i in this.listesFilms) {
        if (this.listesFilms[i].films.find(elem => this.film.id === elem.id)) {
          this.checked[i] = true;
        }
        else {
          this.checked[i] = false;
        }
      }
    });
  }

  updateList(value: boolean, liste: ListFilms) {
    if (value) {
      this.lfs.appendFilmToList({ id: this.film.id, title: this.film.title, posterPath: this.film.posterPath, price: this.film.price, quantity: this.film.quantity }, liste);
    }
    else {
      this.lfs.removeFilmFromList({ id: this.film.id, title: this.film.title, posterPath: this.film.posterPath, price: this.film.price, quantity: this.film.quantity }, liste);
    }
  }

  onClick(): void {
    this.dialogRef.close();
  }
}
