import { Component } from '@angular/core';
import { MovieGenre, MovieResponse } from '../tmdb-data/Movie';
import { MovieResult, SearchMovieQuery } from '../tmdb-data/searchMovie';
import { BasketService, Film } from '../_services/basket.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LoginService } from '../_services/login.service';
import { TmdbService } from '../tmdb.service';
import { environment } from 'src/environments/environment';
import { MatDialog } from '@angular/material/dialog';
import { DialogListComponent } from '../dialog-list/dialog-list.component';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.scss']
})

export class FilmsComponent{
  genres: MovieGenre[] = [];
  searchInput = '';
  currentGenreId: number;
  state = 'init';

  movies: MovieResponse[] = [];
  moviesInBasket: Film[] = [];
  userIsConnected: boolean;

  pageSize = undefined;
  page = 0;

  constructor(private tmdb: TmdbService, private basketService: BasketService, private loginService: LoginService,
              public snackBar: MatSnackBar, public dialog: MatDialog) {
    this.tmdb.init(environment.tmdbKey);
    this.basketService.panierObservable.subscribe(panier => this.moviesInBasket = panier);
    this.loginService.isConnected.subscribe(value => this.userIsConnected = value);

    this.setPopularMovies();
    this.setGenres();
  }

  async setGenres() {
    this.genres = (await this.tmdb.getGenres()).genres;
  }

  private async fromMovieResultsToMovieResponses(tabMovies: MovieResult[]): Promise<MovieResponse[]> {
    const L: (void | MovieResponse)[] = (await Promise.all(tabMovies.map( film =>
      this.tmdb.getMovie(+film.id).catch(error => console.error(film) ) ) ) ).filter(v => v !== undefined);

    this.pageSize = L.length;
    return L as MovieResponse[];
  }

  async setPopularMovies() {
    const popularMovies = (await this.tmdb.getPopularMovies({ page: this.page + 1 })).results;
    this.movies = await this.fromMovieResultsToMovieResponses(popularMovies);
    this.state = 'popular';
  }

  async setNewMovies() {
    const newMovies = (await this.tmdb.getNewMovies({ page: this.page + 1 })).results;
    this.movies = await this.fromMovieResultsToMovieResponses(newMovies);
    this.state = 'new';
  }

  async setMoviesByGenre(genre: string) {
    const id = this.genres.find(element => element.name === genre).id;
    this.currentGenreId = id;
    const moviesWithGenre = (await this.tmdb.discoverMovies({ with_genres: id, page: this.page + 1 })).results;
    this.movies = await this.fromMovieResultsToMovieResponses(moviesWithGenre);
    this.state = 'sorted';
  }

  async onSearch() {
    if(this.searchInput !== '') {
      const query: SearchMovieQuery = { query: this.searchInput, region: 'FR' };
      const moviesResponses = await this.tmdb.searchMovie(query);
      this.movies = await this.fromMovieResultsToMovieResponses(moviesResponses.results);
      this.currentGenreId = undefined;
      this.state = 'search';
      this.page = 0;
    }
  }

  async addMovieToBasket(movie: MovieResponse) {
    if(this.userIsConnected) {
      for(const mov of this.moviesInBasket) {
        if(mov.id === movie.id) {
          this.snackBar.open('Article déjà dans le panier', 'Close');
          return;
        }
      }
      this.snackBar.open('Ajouté au panier', 'Close');
      this.basketService.appendFilmToBasket(movie.id, movie.title, 'https://image.tmdb.org/t/p/w200/' + movie.poster_path, 3.75, 1);
    }
    else {
      this.snackBar.open('Veuillez vous connecter', 'Close');
    }
  }

  getQuantity(id: number) {
    for(const movie of this.moviesInBasket) {
      if(movie.id === id) {
        return movie.quantity;
      }
    }
    return null;
  }

  pageEvents(event: any) {
    if (event.pageIndex > this.page) {
      this.page++;
    }
    else {
      this.page--;
    }
    if (this.state !== 'search') {
      if (this.state === 'sorted') {
        this.setMoviesByGenre(this.genres.find(element => element.id === this.currentGenreId).name);
      }
      else if (this.state === 'init' || 'popular') {
        this.setPopularMovies();
      }
      else if (this.state === 'new') {
        this.setNewMovies();
      }
    }
  }

  openDialogAndAddFilmToList(film) {
    const dialogRef = this.dialog.open(DialogListComponent, {
      data: {
        dataKey: {id: film.id, title: film.title, posterPath: film.poster_path, price: 3.75, quantity: 1}
      }
    });
  }

}
