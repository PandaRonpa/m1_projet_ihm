import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';
import { ListeFilmsService, ListFilms } from '../_services/liste-films.service';
import { LoginService } from '../_services/login.service';

export interface DialogData {
  listName: string;
}

@Component({
  selector: 'app-list-films',
  templateUrl: './list-films.component.html',
  styleUrls: ['./list-films.component.scss']
})
export class ListFilmsComponent {

  public listsFilms: ListFilms[] = [];
  public newListName: string;

  isConnected: boolean;

  constructor(private loginService: LoginService, private listeFilmsService: ListeFilmsService, private dialog: MatDialog) {
    loginService.isConnected.subscribe(value => { this.isConnected = value; });
    listeFilmsService.listsObservable.subscribe(value => { this.listsFilms = value });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '250px',
      data: { listName: this.newListName }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.listeFilmsService.appendListeFilms(result);
      this.newListName = result;
      const list: ListFilms = {
        films: [],
        Label: this.newListName,
      }
      this.listeFilmsService.appendListeFilms(list);
    });
    this.newListName = "";
  }

  deleteFilmOnList(indexFilm: number, indexList: number): void {
    this.listeFilmsService.removeFilmFromList(this.listsFilms[indexList].films[indexFilm], this.listsFilms[indexList]);
  }

  deleteList(indexList: number): void {
    this.listeFilmsService.removeListeFilms(this.listsFilms[indexList]);
  }
}
