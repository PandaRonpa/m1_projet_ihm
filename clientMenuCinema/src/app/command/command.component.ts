import { AfterContentChecked, AfterContentInit, Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { ListCommand } from '../_interfaces/command';
import { CommandService } from '../_services/command.service';
import { LoginService } from '../_services/login.service';

@Component({
  selector: 'app-command',
  templateUrl: './command.component.html',
  styleUrls: ['./command.component.scss']
})
export class CommandComponent implements AfterContentChecked{

  searchText;
  histoCommand: ListCommand = null;

  /**
   * @constructor
   * @param cmds Service which is in charge of the command database
   * @param login Service which is in charge of the connection of the user
   * @since 20/01/2021
   */
  constructor(private cmds: CommandService, private login: LoginService) {}

  ngAfterContentChecked(): void {
    if(this.login.isConnected.value){ this.histoCommand = this.cmds.getcommandsList();} 
    else{this.histoCommand = null;this.cmds.reset();}
  }

  isConnected(): boolean {return this.login.isConnected.value;}

  nbCommand(): number{ return this.histoCommand?.totalCommands ?? 0; }
}
