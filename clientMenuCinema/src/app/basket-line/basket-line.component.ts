import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-basket-line',
  templateUrl: './basket-line.component.html',
  styleUrls: ['./basket-line.component.scss']
})

export class BasketLineComponent {
  @Input() codeArticle: number;
  @Input() nomArticle: string;
  @Input() photoArticle: string;
  @Input() prixArticle: number;
  @Input() quantiteArticle: number;
  @Input() typeArticle: string;

  constructor() {}

  getPrixLigne() {
    const resultat = this.prixArticle * this.quantiteArticle;
    return resultat;
  }

  getCode() {
    return this.codeArticle;
  }

  getNom() {
    return this.nomArticle;
  }

  getPhoto() {
    return this.photoArticle;
  }

  getQte() {
    return this.quantiteArticle;
  }

  getPrix() {
    return this.prixArticle;
  }

  getType() {
    return this.typeArticle;
  }

  setCode(code) {
    this.codeArticle = code;
  }

  setNom(nom) {
    this.nomArticle = nom;
  }

  setPhoto(photo) {
    this.photoArticle = photo;
  }

  setQuantite(quantite) {
    this.quantiteArticle = quantite;
  }

  setPrix(prix) {
    this.prixArticle = prix;
  }

  setType(type) {
    this.typeArticle = type;
  }

}
