import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasketLineComponent } from './basket-line.component';

describe('BasketLineComponent', () => {
  let component: BasketLineComponent;
  let fixture: ComponentFixture<BasketLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasketLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasketLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
