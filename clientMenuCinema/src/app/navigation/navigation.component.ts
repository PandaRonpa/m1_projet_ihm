import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { LoginService } from '../_services/login.service';
import { BasketService, Film } from '../_services/basket.service';

@Component({
  selector: 'app-nav-app',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {
  profilePicture: string;
  panier: Film[] = [];

  constructor(private loginService: LoginService, private basketService: BasketService, private titleService: Title) {
    loginService.isConnected.subscribe(value => {
      if (value === true) {
        this.profilePicture = loginService.profilePicture;
      }
    });
    basketService.panierObservable.subscribe(panier => this.panier = panier);
  }

  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }

  get isConnectedObservable() {
    return this.loginService.isConnected.asObservable();
  }

  getQuantity() {
    return this.panier.length;
  }
}
