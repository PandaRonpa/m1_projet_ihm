// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  tmdbKey: '367495ab39a543bb0d011be335e0194f',
  firebase: {
    apiKey: 'AIzaSyCGteFnFlyXIEFPQjyBOpvxQ1bpUj2bYm0',
    authDomain: 'menucinema-pcrvb.firebaseapp.com',
    databaseURL: 'https://menucinema-pcrvb.firebaseio.com',
    projectId: 'menucinema-pcrvb',
    storageBucket: 'menucinema-pcrvb.appspot.com',
    messagingSenderId: '800614088557',
    appId: '1:800614088557:web:873d078069e70e8287e464',
    measurementId: 'G-6SH9B48VYS'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
